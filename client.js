/* ******************************************************************
 * Constantes de configuration
 */
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion des tabs "Voter" et "Toutes les citations"
 ******************************************************************** */

/**
 * Affiche/masque les divs "div-duel" et "div-tout"
 * selon le tab indiqué dans l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majTab(etatCourant) {
  console.log("CALL majTab");
  const dDuel = document.getElementById("div-duel");
  const dTout = document.getElementById("div-tout");
  const tDuel = document.getElementById("tab-duel");
  const tTout = document.getElementById("tab-tout");
  if (etatCourant.tab === "duel") {
    dDuel.style.display = "flex";
    tDuel.classList.add("is-active");
    dTout.style.display = "none";
    tTout.classList.remove("is-active");
  } else {
    dTout.style.display = "flex";
    tTout.classList.add("is-active");
    dDuel.style.display = "none";
    tDuel.classList.remove("is-active");
  }
}

/**
 * Mets au besoin à jour l'état courant lors d'un click sur un tab.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} tab le nom du tab qui a été cliqué
 * @param {Etat} etatCourant l'état courant
 */
function clickTab(tab, etatCourant) {
  console.log(`CALL clickTab(${tab},...)`);
  if (etatCourant.tab !== tab) {
    etatCourant.tab = tab;
    majPage(etatCourant);
  }
}

/**
 * Enregistre les fonctions à utiliser lorsque l'on clique
 * sur un des tabs.
 *
 * @param {Etat} etatCourant l'état courant
 */
function registerTabClick(etatCourant) {
  console.log("CALL registerTabClick");
  document.getElementById("tab-duel").onclick = () =>
    clickTab("duel", etatCourant);
  document.getElementById("tab-tout").onclick = () =>
    clickTab("tout", etatCourant);
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami() {
  var apiKey = document.getElementById("api_user").value;
  return fetch(serverUrl + "/whoami", { headers: { "x-api-key": apiKey } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans
 * la modale d'affichage de l'utilisateur.
 *
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(etatCourant) {
  return fetchWhoami().then((data) => {
    etatCourant.login = data.login; // qui vaut undefined en cas d'erreur
    etatCourant.errLogin = data.err; // qui vaut undefined si tout va bien
    majPage(etatCourant);
    // Une promesse doit renvoyer une valeur, mais celle-ci n'est pas importante
    // ici car la valeur de cette promesse n'est pas utilisée. On renvoie
    // arbitrairement true
    return true;
  });
}

/**
 * Affiche ou masque la fenêtre modale de login en fonction de l'état courant.
 * Change la valeur du texte affiché en fonction de l'état
 *
 * @param {Etat} etatCourant l'état courant
 */
function majModalLogin(etatCourant) {
  const modalClasses = document.getElementById("mdl-login").classList;
  if (etatCourant.loginModal) {
    modalClasses.add("is-active");
    const elt = document.getElementById("elt-affichage-login");
	
	
    const ok = etatCourant.login !== undefined;
    
    
    if (!ok) {
	  	
      elt.innerHTML = `<span class="is-error">${etatCourant.errLogin}</span>`;
    } else {
	  
      elt.innerHTML = `Bonjour ${etatCourant.login}.`;
    }
  } else {
    modalClasses.remove("is-active");
  }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickFermeModalLogin(etatCourant) {
  etatCourant.loginModal = false;
  majPage(etatCourant);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickOuvreModalLogin(etatCourant) {
  etatCourant.loginModal = true;
  lanceWhoamiEtInsereLogin(etatCourant);
  majPage(etatCourant);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant l'utilisateur.
 * @param {Etat} etatCourant
 */
function registerLoginModalClick(etatCourant) {
  document.getElementById("btn-close-login-modal1").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-close-login-modal2").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-open-login-modal").onclick = () =>
    clickOuvreModalLogin(etatCourant);
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  majTab(etatCourant);
  majModalLogin(etatCourant);
  registerTabClick(etatCourant);
  registerLoginModalClick(etatCourant);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */

 
function initClientCitations() {
  console.log("CALL initClientCitations");
  const etatInitial = {
    tab: "duel",
    loginModal: false,
    login: undefined,
    errLogin: undefined,
  };
  majPage(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientCitations();
});

/** Affichage de toutes les citations à partir du serveur
 * 
 *  
 */
function afficher(callback){
	    var requestURL = 'https://lifap5.univ-lyon1.fr/citations';
		var request = new XMLHttpRequest();
		request.open('GET', requestURL,true);
		request.responseType = 'json';
		
		request.onload = function(){
			var result = request.response;
			var status = request.status;
            if (status === 200) {
				callback(null, result);
	     	} else {
				callback(status, result);
			  }		 
		};
	    
	    request.send();
}

AllQuotes = function(err, data) {
      if (err !== null) {
         alert('Something went wrong: ' + err);
      } else {
			
		  var tblBody = document.getElementById("tblBody");
		  
		  for(var i=0 ; i<data.length ;i++){
		 
		  var tblTr = document.createElement("tr");
		 
		  var tblTd1 = document.createElement("td");
		  var tblTd2 = document.createElement("td");
		  var tblTd3 = document.createElement("td");
		 
		  tblTd1.innerHtml = data[i].character ;
		  tblTd2.innerHTML = data[i].character;		
		  tblTd3.innerHTML = data[i].quote;	
		  		           
	      tblTr.appendChild(tblTd1);
	      tblTr.appendChild(tblTd2);
	      tblTr.appendChild(tblTd3);
		 
		  tblBody.appendChild(tblTr);
	   }  
   }
}

/** Generer 2 citations au hasard
 * 
 *  
 */

initQuotes = function(err, data) {
      if (err !== null) {
         alert('Something went wrong: ' + err);
      } else {
		 
		 
		 var j = Math.floor((Math.random() * data.length) + 0);
		 data.splice(j,1);
		 var b = Math.floor((Math.random() * (data.length -1)) + 0);
     	 
		 var leftQuote = document.getElementById("lQ");
		 var rightQuote = document.getElementById("rQ");
		 var LFigure = document.getElementById("imgLeft");
		 var RFigure = document.getElementById("imgRight");
		 
		 var pQuoteL = document.createElement("p");
		 var pCharacterL = document.createElement("p");
		 var imgL = document.createElement("IMG");
		 
		 var pQuoteR = document.createElement("p");
		 var pCharacterR = document.createElement("p");
		 var imgR = document.createElement("IMG");
		 
		 pQuoteL.innerHTML = data[j].quote;
		 pQuoteL.className = "title";
		 pCharacterL.innerHTML = data[j].character;
		 pCharacterL.className = "subtitle" ;
		 imgL.src = data[j].image ;
		 
		 pQuoteR.innerHTML = data[b].quote;
		 pQuoteR.className = "title";
		 pCharacterR.innerHTML = data[b].character;
		 pCharacterR.className = "subtitle" ;
		 imgR.src = data[b].image ;
		
		 leftQuote.appendChild(pQuoteL);
		 leftQuote.appendChild(pCharacterL);
		 LFigure.appendChild(imgL);
		 
		 rightQuote.appendChild(pQuoteR);
		 rightQuote.appendChild(pCharacterR);
		 RFigure.appendChild(imgR);
		 
   }
}


afficher(initQuotes);
afficher(AllQuotes);


function connexion(){
	
	var btn = document.getElementById("btnX");
	var api_cle = document.getElementById("api_user");
	var txt = document.getElementById("txt2");
	var result = document.getElementById("elt-affichage-login")
	
	
	
	if(2<3){
	btn.remove();
	api_cle.remove(); 
	txt2.remove();
	
	}
	else{
	result.innerHTML ="try Again!"	
	
	}

}

function vote(){
	
	var lVote = document.getElementById(leftVote);
	var rVote = document.getElementById(rightVote);
    
    
    
}


	

